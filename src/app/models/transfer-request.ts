export class TransferRequest {
  constructor(
    public accountTargetId: string,
    public amount: string
  ) {
  }
}