import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { PersonAccount } from '../models/person-account';
import { TransferRequest } from '../models/transfer-request';
import { UserAccount } from '../models/user-account';

@Injectable({
  providedIn: 'root'
})
export class BankService {

  constructor(private http: HttpClient) { }

  userAccount(){
    return this.http.get<UserAccount>(`${environment.apiUrl}/user-account`);
  }

  accounts() {
    return this.http.get<PersonAccount[]>(`${environment.apiUrl}/list-accounts`);
  }

  transfer(transferRequest:TransferRequest){
    return this.http.post<void>(`${environment.apiUrl}/transfer`, transferRequest);
  }
}
