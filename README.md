# Banking application frontend

## Requirements
- Node 14.17.2+
- Angular CLI 12.1.1+
- Banking application backend running in port 8080

## How to run
In terminal execute:

```
npm install
ng serve --open
```
