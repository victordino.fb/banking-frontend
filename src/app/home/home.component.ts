import { Component, OnInit } from '@angular/core';
import { count, first } from 'rxjs/operators';
import { User } from '../models/user';
import { UserAccount } from '../models/user-account';
import { AccountService } from '../services/account.service';
import { BankService } from '../services/bank.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  user: User | null;
  userAccount?: UserAccount;
  
  constructor(
    private accountService: AccountService,
    private bankService: BankService
    ) {
    this.user = this.accountService.userValue;
    this.bankService.userAccount()
    .pipe(first())
    .subscribe({
      next: (userAccount) => {
        this.userAccount = userAccount;
      },
      error: error => {
        this.user = null;
      }
    });
  }

  ngOnInit(): void {
  }

}
