export class PersonAccount {
  constructor(
    public firstName: string,
    public lastName: string,
    public accountId: string
  ) {
  }
}