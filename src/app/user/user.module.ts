import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { TransferComponent } from './transfer/transfer.component';
import { TransferDetailsComponent } from './transfer-details/transfer-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TransferResultComponent } from './transfer-result/transfer-result.component';

@NgModule({
  declarations: [
    LayoutComponent,
    TransferComponent,
    TransferDetailsComponent,
    TransferResultComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    UserRoutingModule
  ]
})
export class UserModule { }
