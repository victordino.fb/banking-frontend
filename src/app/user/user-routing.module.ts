import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { TransferDetailsComponent } from './transfer-details/transfer-details.component';
import { TransferResultComponent } from './transfer-result/transfer-result.component';
import { TransferComponent } from './transfer/transfer.component';

const routes: Routes = [
  {
    path: '', component: LayoutComponent,
    children: [
      { path: 'transfer', component: TransferComponent },
      { path: 'transfer-details', component: TransferDetailsComponent },
      { path: 'transfer-result', component: TransferResultComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
