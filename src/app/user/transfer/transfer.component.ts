import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { PersonAccount } from 'src/app/models/person-account';
import { BankService } from 'src/app/services/bank.service';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit {

  accounts: PersonAccount[] = []

  constructor(private router: Router, private bankService: BankService) { }

  ngOnInit(): void {
    this.bankService.accounts()
      .pipe(first())
      .subscribe(accounts => this.accounts = accounts);
  }

  transfer(account: PersonAccount) {
    this.router.navigate(['/user/transfer-details'], { state: { account: account } });
  }
}
