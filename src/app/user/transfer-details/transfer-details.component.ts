import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { PersonAccount } from 'src/app/models/person-account';
import { TransferRequest } from 'src/app/models/transfer-request';
import { AlertService } from 'src/app/services/alert.service';
import { BankService } from 'src/app/services/bank.service';

@Component({
  selector: 'app-transfer-details',
  templateUrl: './transfer-details.component.html',
  styleUrls: ['./transfer-details.component.css']
})
export class TransferDetailsComponent implements OnInit {
  form: FormGroup;
  loading = false;
  submitted = false;

  warningMessage?:String

  account: PersonAccount

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private bankService: BankService,
    private alertService: AlertService) {
    const nav = this.router.getCurrentNavigation();
    this.account = nav?.extras?.state?.account;
    if (this.account) {
      this.form = this.formBuilder.group({
        amount: ['', [Validators.required, Validators.pattern("[0-9]*"), Validators.min(0)]]
      });
    } else {
      this.form = this.formBuilder.group({});
      this.router.navigate(['/user/transfer']);
    }
  }

  ngOnInit(): void {
  }

  get f() { return this.form.controls; }

  onSubmit() {
    this.submitted = true;

    // reset alerts on submit
    this.alertService.clear();

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    const formValue = this.form.value;
    const accountId = this.account?.accountId;
    console.log("accountId", accountId);
    if (accountId != null) {
      console.log(formValue.amount);
      this.bankService.transfer(new TransferRequest(accountId, formValue.amount))
        .pipe(first())
        .subscribe({
          next: () => {
            this.alertService.success('Transfer successful', { keepAfterRouteChange: true });
            this.router.navigate(['/user/transfer-result'])
          },
          error: error => {
            this.warningMessage = error;
            this.alertService.error(error);
            this.loading = false;
          }
        });
    }
  }
}
